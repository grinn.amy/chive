;;; chive.el --- Alternate archive for Org -*- lexical-binding: t -*-

;; Copyright (C) 2024 Amy Grinn

;; Author: Amy Grinn <grinn.amy@gmail.com>
;; Version: 0.0.2
;; Package-Requires: ((emacs "27.2"))
;; File: chive.el
;; Keywords: tools
;; URL: https://gitlab.com/grinn.amy/chive

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;;; Commentary:

;; Chive provides an alternative archive style for Org mode files.
;; Each heading's direct ancestors will be recreated in the archive
;; file.  The subtree itself will be merged into whatever exists
;; already in the archive file.  In essence, the archive file will
;; look like the same Org file it came from.
;;
;; To enable chive-mode, add this to your init file:
;;
;;   (add-hook 'org-mode-hook 'chive-mode)
;;
;; An overview of when things were archived can be seen by pressing [
;; from an agenda buffer limited to the archive file.
;; 
;; The archive file and parent heading is determined by the variable
;; `org-archive-location'.  However, using the keyword `datetree' in
;; the archive location is not supported.

;;; Code:

;;;; Requirements

(require 'org-archive)
(require 'org-refile)

;;;; Commands

;;;###autoload
(defun chive-archive-subtree ()
  "Move the entry at point to the archive file."
  (interactive)
  (let* ((loc (org-archive--compute-location
	       (or (org-entry-get nil "ARCHIVE" 'inherit)
		   org-archive-location)))
	 (buf (chive--setup-archive (car loc)))
	 (parent-path (with-current-buffer buf
			(chive--find-heading-insert (cdr loc))))
	 (new-pos (chive--merge-subtree-into parent-path buf)))
    (chive--log-note new-pos buf)
    (org-copy-subtree 1 t)))

;;;; Modes

;;;###autoload
(define-minor-mode chive-mode
  "Remap \\<org-mode-map>\\[org-archive-subtree] to the command `chive-archive-subtree'."
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map
                        (kbd "<remap> <org-archive-subtree>")
                        #'chive-archive-subtree)
            map))

;;;; Utility expressions

(defun chive--find-path (path)
  "Return the position of the heading described by PATH."
  (catch 'found
    (org-map-entries
     (lambda ()
       (when (equal path (org-get-outline-path t))
	 (throw 'found (point))))
     (format "LEVEL=%d" (length path)))
    nil))

(defun chive--find-path-create (path)
  "Find or create a heading at PATH and return its position."
  (let ((i 0)
	(len (length path))
	pos loc)
    (while (and (< i len)
		(not (setq pos (chive--find-path (butlast path i)))))
      (setq i (1+ i)))
    (setq loc (list (nth (- len i) path) (buffer-file-name) nil pos))
    (while (> i 0)
      (setq loc (org-refile-new-child loc (nth (- len i) path)))
      (setq i (1- i)))
    (nth 3 loc)))

(defun chive--find-heading-insert (heading)
  "Find HEADING string in current buffer or insert it at the end."
  (unless (string-empty-p heading)
    (save-excursion
      (goto-char (point-min))
      (unless (re-search-forward
	       (concat "^" (regexp-quote heading)) nil t)
	;; Insert heading at end
	(goto-char (point-max))
	(or (bolp) (insert "\n"))
	(insert "\n" heading "\n"))
      (org-get-outline-path t))))

(defun chive--merge-subtree-into (parent-path buffer)
  "Recreate the subtree at point under PARENT-PATH in BUFFER."
  (let ((path (append parent-path (org-get-outline-path))))
    (with-current-buffer buffer
      (chive--find-path-create path)))
  (let (first)
    (org-map-entries
     (lambda ()
       (let* ((path (append parent-path (org-get-outline-path t)))
	      (beg (prog1 (point) (end-of-line)))
	      (end (or (and (re-search-forward
			     org-heading-regexp nil t)
			    (match-beginning 0))
		       (point-max)))
	      (str (buffer-substring beg end)))
	 (with-current-buffer buffer
	   (save-excursion
	     (goto-char (chive--find-path-create path))
	     (unless first (setq first (point)))
             (delete-region (point) (progn (end-of-line) (point)))
	     (insert str)))))
     nil 'tree)
    first))

(defun chive--setup-archive (file)
  "Set up the the archive FILE to be in `org-mode'."
  (let ((buf (or (find-buffer-visiting file)
		 (find-file-noselect file))))
    (with-current-buffer buf
      (when (= (buffer-size) 0)
	(insert "# -*-org-*-\n"))
      (unless (eq major-mode 'org-mode)
	(let ((org-inhibit-startup t))
	  (org-mode)))
      (setq-local chive---archive t))
    buf))

(defun chive--log-note (position buffer)
  "Add an archive note to the heading at POSITION in BUFFER."
  (let ((org-log-note-purpose 'archive)
	(org-log-note-headings '((archive . "Archived on %t")))
	(org-log-note-effective-time (org-current-effective-time))
	(org-log-note-how 'time)
	(org-log-note-window-configuration
	 (current-window-configuration)))
    (move-marker org-log-note-marker position buffer)
    (move-marker org-log-note-return-to (point))
    (with-temp-buffer (org-store-log-note))))


(provide 'chive)
;;; chive.el ends here
